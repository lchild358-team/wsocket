module.define('wsocket', (function(){

  const Cls = function(ws){
    this._ws = ws;

    this._onopens     = [];
    this._onerrors    = [];
    this._onmessages  = [];
    this._ondatas     = [];
    this._onjsondatas = [];
    this._oncloses    = [];

    this._reopen_on_close = false;
    this._error_flg       = false;

    this._sendcache = [];
  };

  Cls.prototype.onOpen = function(f, context){
    this._onopens.push({
      callback: f,
      context : context
    });
    return this;
  };

  Cls.prototype.onError = function(f, context){
    this._onerrors.push({
      callback: f,
      context : context
    });
    return this;
  };

  Cls.prototype.onMessage = function(f, context){
    this._onmessages.push({
      callback: f,
      context : context
    });
    return this;
  };

  Cls.prototype.onData = function(f, context){
    this._ondatas.push({
      callback: f,
      context : context
    });
    return this;
  };

  Cls.prototype.onJsonData = function(f, context){
    this._onjsondatas.push({
      callback: f,
      context : context
    });
    return this;
  };

  Cls.prototype.onClose = function(f, context){
    this._oncloses.push({
      callback: f,
      context : context
    });
    return this;
  };

  Cls.prototype.readyState = function(){
    return this._ws.readyState;
  };

  Cls.prototype.isOpen = function(){
    return this._ws.readyState == this._ws.OPEN;
  };

  Cls.prototype.isClosed = function(){
    return this._ws.readyState == this._ws.CLOSED;
  };

  Cls.prototype.isConnecting = function(){
    return this._ws.readyState == this._ws.CONNECTING;
  };

  Cls.prototype.isClosing = function(){
    return this._ws.readyState == this._ws.CLOSING;
  };

  Cls.prototype.isClosed = function(){
    return this._ws.readyState == this._ws.CLOSED;
  };

  /**
   * @param url String (optional)
   */
  Cls.prototype.open = function(url){
    if(/^ws:\/\/.+$/.test(url)){
      this._url = url;
    }else if(/^\/.+$/.test(url)){
      this._url = `ws:${window.location.hostname}:${window.location.port}${url}`;
    }

    this._ws = new WebSocket(this._url);

    const me = this;
    this._ws.onopen = function(){
      /* */ console.info(`Connection established (${me._url})`);
      for(var f of me._onopens){
        f.callback.apply(f.context ? f.context : me, arguments);
      }

      me.flush();
    };

    this._ws.onerror = function(e){
      /* */ console.error(`Connection failed (${me._url})`);
      for(var f of me._onerrors){
        f.callback.apply(f.context ? f.context : me, arguments);
      }

      me._error_flg = true;
    };

    this._ws.onmessage = function(e){
      /* */ console.debug(`Message received (${e.origin})`);
      /* */ console.debug(e);

      for(var f of me._onmessages){
        f.callback.apply(f.context ? f.context : me, arguments);
      }

      for(var f of me._ondatas){
        f.callback.apply(f.context ? f.context : me, [e.data]);
      }

      if(me._onjsondatas.length > 0){
        let jsondata = false;
        try{
          jsondata = JSON.parse(e.data);
        }catch(err){
          /* */ console.warn('Not JSON data');
          /* */ console.debug(err);
        }

        if(jsondata){
          for(var f of me._onjsondatas){
            f.callback.apply(f.context ? f.context : me, [jsondata]);
          }
        }
      }
    };

    this._ws.onclose = function(){
      /* */ console.info(`Connection closed (${me._url})`);
      for(var f of me._oncloses){
        f.callback.apply(f.context ? f.context : me, arguments);
      }

      if(! me._error_flg && me._reopen_on_close){
        me.open(me._url);
      }
    };

    return this;
  };

  Cls.prototype.close = function(){
    this._ws.close();
    return this;
  };

  /**
   * @param value Boolean
   */
  Cls.prototype.reopenOnClose = function(value){
    this._reopen_on_close = value;
    return this;
  };

  /**
   *
   */
  Cls.prototype.reopenIfClosed = function(){
    if(this._ws.readyState == this._ws.CLOSED){
      this.open(this._url);
    }
    return this;
  };

  /**
   * @param message String | Object
   */
  Cls.prototype.send = function(message){
    this._sendcache.push(message);
    return this.flush();
  };

  Cls.prototype.flush = function(){
    if(this._sendcache.length <= 0) return this;
    if(! this.isOpen()) return this;

    let message = this._sendcache.splice(0, 1)[0];

    if(typeof(message) === 'object' && message !== null){
      /* */ console.info(`Send message (object) to '${this._url}'`);
      /* */ console.debug(message);
      this._ws.send(JSON.stringify(message));
    }else if(typeof(message) === 'string'){
      /* */ console.info(`Send message (${message}) to '${this._url}'`);
      this._ws.send(message);
    }else{
      /* */ console.info(`Send message (${message}) to '${this._url}'`);
      this._ws.send(new String(message));
    }

    return this.flush();
  };

  const func = function(url){
    return new Cls().open(url);
  };

  return func;

})());
