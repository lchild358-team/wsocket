# wsocket
Simple wrapper of web socket on website.

## Include
```html
<script src="node_modules/web-module/web-module.js"></script>
<script src="node_modules/wsocket/wsocket.js"></script>
```

## Usage
```javascript
const wsocket = module.require('wsocket');
let ws = wsocket('/web/socket/uri');

ws.onOpen((event)=>{
  // Do something
}).onError((event)=>{
  // Do something
}).onMessage((event)=>{
  // Do something
}).onData((data)=>{
  // Do something with raw data
}).onJsonData((data)=>{
  // Do something with json data
});
```

## Events

### onOpen(*f*, *context*)

##### Parameters:
* **f** function(*event*)
* **context** object (optional)

##### Example:
```javascript
ws.onOpen((event)=>{
  // Do something
}, context);
```
### onError(*f*, *context*)

##### Parameters:
* **f** function(*event*)
* **context** object (optional)

##### Examples:
```javascript
ws.onError((event)=>{
  // Do something
}, context);
```

### onMessage(*f*, *context*)

##### Parameters:
* **f** function(*event*)
* **context** object (optional)

##### Examples:
```javascript
ws.onMessage((event)=>{
  // Do something
}, context);
```

### onData(*f*, *context*)

##### Parameters:
* **f** function(*data*)
* **context** object (optional)

##### Examples:
```javascript
ws.onData((data)=>{
  // Do something with raw data
}, context);
```

### onJsonData(*f*, *context*)

##### Parameters:
* **f** function(*data*)
* **context** object (optional)

##### Examples:
```javascript
ws.onJsonData((data)=>{
  // Do something with json data
}, context);
```

### onClose(*f*, *context*)

##### Parameters:
* **f** function(*event*)
* **context** object (optional)

##### Examples:
```javascript
ws.onClose((event)=>{
  // Do something
}, context);
```

## Ready state

### isConnecting()
Check if the websocket is on CONNECTING state.

### isOpen()
Check if the websocket is on OPEN state.

### isClosing()
Check if the websocket is on CLOSING state.

### isClosed()
Check if the websocket is on CLOSED state.

### readyState()
Get websocket ready state.

## Others

### open(*uri*)
Open websocket with other uri.

### close()
Close current websocket.

### reopenOnClose()
Set up websocket to automatically reopen when closed.

##### Examples:
```javascript
let ws = wsocket('/web/socket/uri').reopenOnClose();
```

### reopenIfClosed()
Reopen current websocket if closed.

### send(*message*)

##### Parameters:
* **message** string | object | mixed

##### Examples:
```javascript
// Send string
ws.send('some string');

// Send json data
ws.send({
  item: 'some data',
  another: 'another data'
});

// Other types of data will be convert to string
ws.send(12345);
```
## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

## License
[ISC](https://opensource.org/licenses/ISC)
